import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MetaLiteSQLDevice {
    private String TargetIP = null;
    private int TargetPort = 0;
    private String username = null;
    private String password = null;
    public MetaLiteSQLDevice(String target_ip,int target_port) {
        this.TargetIP = target_ip;
        this.TargetPort = target_port;
    }
    public boolean login(String username , String password) throws Exception{
        URL url = new URL(TargetIP+":"+TargetPort+"/"+username+"/"+password+"/login");
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.connect();
        InputStream inputStream = httpURLConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            String line = bufferedReader.readLine();
            if (line == null) {
                break;
            }
            stringBuilder.append(line);
        }
        String message = stringBuilder.toString();
        return message.toLowerCase().contains("login successful");
    }
    public String exec(String command) throws Exception {
        URL url = new URL(TargetIP+":"+TargetPort+"/"+username+"/"+password+"/"+command);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.connect();
        InputStream inputStream = httpURLConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            String line = bufferedReader.readLine();
            if (line == null) {
                break;
            }
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }
}
